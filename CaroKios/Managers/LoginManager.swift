//
//  LoginManager.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

class LoginManager {
    
    /// Decide if user needs to login or not
    static func login() {
        if !DataManager.shared.token.isEmpty {
            showMainView()
        } else if let token = UserDefaults.standard.string(forKey: kToken), !token.isEmpty {
            showMainView()
        } else {
            showLoginView()
        }
    }
    
    /// Show main view
    static func showMainView() {
        let tabbarVC = TabBarViewController()
        show(viewController: tabbarVC)
    }
    
    /// Show login view
    static func showLoginView() {
        let loginViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        show(viewController: loginViewController)
    }
    
    /// Show view based on viewController
    ///
    /// - Parameter viewController: viewController needs to be displayed
    private static func show(viewController: UIViewController) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.window?.rootViewController = viewController
    }
}
