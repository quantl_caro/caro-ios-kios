//
//  DataManager.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

class DataManager {
    static let shared = DataManager()
    private init() {}
    
    var userInfo: UserInfoModel?
    var memberCount: Int = 0
    var memberList: [MemberModel] = []
    var level: BrokerLevel?
    var combinedIncomeList: [CombinedIncomeModel] = []
    
    var currentSelectedMonth: Date = Date()
    var stepFromCurrentMonth: Int = 0
    
    var incomeList: IncomeModel? {
        didSet {
            guard let detailList = self.incomeList?.details else {
                return
            }
            
            _ = detailList.map { detail in
                memberList.map { member in
                    if detail.name == member.user_name {
                        let combined = CombinedIncomeModel(detail: detail, address: member.address)
                        combinedIncomeList.append(combined)
                    }
                }
            }
        }
    }
    
    var token: String = "" {
        didSet {
            UserDefaults.standard.set(token, forKey: kToken)
        }
    }
    
    var loginName: String = "" {
        didSet {
            UserDefaults.standard.set(loginName, forKey: kUsername)
        }
    }
    
    var extraUserInfo: ExtraUserInfoModel? {
        didSet {
            if let levelString = self.extraUserInfo?.level {
                level = BrokerLevel.init(rawValue: levelString)
                return
            }
        }
    }
    
    func updateHistory() {
//        APIClient.shared.getIncomeByAccount(from: <#T##String#>, to: <#T##String#>)
    }
    
    func login(userName id: String, password pass: String) {
        APIClient.shared.login(userName: id, password: pass)
            .done(on: DispatchQueue.main) { data in
                DataManager.shared.loginName = id
                LoginManager.showMainView()
            }
            .catch { error in }
    }
    
    func clearDataForLogout() {
        userInfo = nil
        memberCount = 0
        memberList = []
        incomeList = nil
        combinedIncomeList = []
        level = nil
        token = ""
        loginName = ""
        extraUserInfo = nil
        currentSelectedMonth = Date()
        stepFromCurrentMonth = 0
    }
    
    // MARK: - For testing
    var pricing: PriceModel?
    var listPricing: [PriceModel] = []
    var resultBooking: ResultBookingModel?
    var reResultBooking: ResultBookingModel?
    var listBooking: [ResultBookingModel] = []
    var deleteReturnMessage: String?
}
