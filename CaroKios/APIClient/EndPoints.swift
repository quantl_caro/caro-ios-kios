//
//  EndPoints.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

enum Host {
    // Development
    static let alphaURL = "https://uat.carotech.info/partner/"
    
    // Product
}

enum EndPoint {
    
    enum Common {
        static let login = "broker-point/login"
        static let logout = "broker-point/logout"
        static let userInfo = "broker-point/user-info"
        static let countMembers = "broker-point/count-members"
        static let accountsInfo = "broker-point/accounts"
        static let pickingPoints = "broker-point/picking-points"
        static let firebaseToken = "broker-point/set-firebase-token"
    }
    
    enum Pricing {
        static let getPricing = "broker-point/pricings"
        static let listPricings = "broker-point/list-pricings"
    }
    
    enum Booking {
        static let booking = "broker-point/bookings"
        static let reBooking = "broker-point/re-bookings"
        static let listBookings = "broker-point/bookings"
        static let deleteBooking = "broker-point/bookings"
    }
    
    enum Income {
        static let incomeByBooking = "broker-point/incomes"
        static let incomeByAccounts = "broker-point/income-by-accounts"
    }
    
    enum MapService {
        
    }
}
