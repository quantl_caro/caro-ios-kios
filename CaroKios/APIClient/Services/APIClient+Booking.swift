//
//  APIClient+Booking.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

extension APIClient {
    
    /// Book a ride
    ///
    /// - Parameter request: contains infomation about the ride
    /// - Returns: Promise with booking infomations received from server
    func booking(with request: RequestBookingModel) -> Promise<ResultBookingModel> {
        let params: [String: Any] = [
            "source": request.source ?? "",
            "service_code": request.service_code ?? "",
            "payment": request.payment ?? "",
            "start_time": request.start_time ?? 0,
            "description": request.description ?? "",
            "destinations": request.destinations ?? [],
            "booking_type": request.booking_type ?? "",
            "estimate_distance": request.estimate_distance ?? 0.0,
            "estimate_time": request.estimate_time ?? 0.0
        ]
        
        return Promise<ResultBookingModel> { seal in
            requestPostWithEndPoint(endPoint: EndPoint.Booking.booking, params: params, isShowLoadingView: false)
                .done { data in
                    let response = try self.decoder.decode(ResultBookingModel.self, from: data)
                    // TO-DO: Handle booking data
                    DataManager.shared.resultBooking = response
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Re-booking
    ///
    /// - Parameter request: contains infomations about the ride
    /// - Returns: Promise with booking infomations received from server
    func reBooking(with request: ReBookingModel) -> Promise<ResultBookingModel> {
        let params: [String: Any] = [
            "old_booking_uid": request.old_booking_uid ?? "",
            "source": request.source ?? "",
            "service_code": request.service_code ?? "",
            "payment": request.payment ?? "",
            "start_time": request.start_time ?? 0,
            "description": request.description ?? "",
            "destinations": request.destinations ?? [],
            "booking_type": request.booking_type ?? "",
            "estimate_distance": request.estimate_distance ?? 0.0,
            "estimate_time": request.estimate_time ?? 0.0
        ]
        
        return Promise<ResultBookingModel> { seal in
            requestPostWithEndPoint(endPoint: EndPoint.Booking.reBooking, params: params, isShowLoadingView: false)
                .done { data in
                    let response = try self.decoder.decode(ResultBookingModel.self, from: data)
                    // TO-DO: Handle booking data
                    DataManager.shared.reResultBooking = response
                    seal.fulfill(response)
                }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Get list of bookings
    ///
    /// - Parameter status: status to filter
    /// - Returns: Promise with list of bookings in ResultBookingModel
    func getListBookings(withStatus status: BookingStatus) -> Promise<[ResultBookingModel]> {
        let finalEndPoint = EndPoint.Booking.listBookings + "?status=\(status)"
        
        return Promise<[ResultBookingModel]> { seal in
            getDataWithEndPoint(endPoint: finalEndPoint, isShowLoadingView: false)
                .done { data in
                    let response = try self.decoder.decode([ResultBookingModel].self, from: data)
                    // TO-DO: Handle bookings data
                    DataManager.shared.listBooking = response
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Delete booking with ID
    ///
    /// - Parameter id: ID of booking
    /// - Returns: Promise with message
    func deleteBooking(withId id: String) -> Promise<String> {
        let finalEndPoint = EndPoint.Booking.deleteBooking + "/\(id)"
        return Promise<String> { seal in
            requestPostWithEndPoint(endPoint: finalEndPoint, params: [:], isShowLoadingView: false, httpType: .DELETE)
                .done { data in
                    let response = try self.decoder.decode(String.self, from: data)
                    // TO-DO: Handle return message
                    DataManager.shared.deleteReturnMessage = response
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
}
