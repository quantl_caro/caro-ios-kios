//
//  APIClient+Common.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

extension APIClient {
    /// Login service
    ///
    /// - Parameters:
    ///   - id: user's id
    ///   - pass: user's password
    /// - Returns: Promise with LoginModel contains token and extraUserInfo
    func login(userName id: String, password pass: String) -> Promise<LoginModel> {
        let params = [
            "user_name": id,
            "password": pass
        ]
        
        return Promise<LoginModel> { seal in
            requestPostWithEndPoint(endPoint: EndPoint.Common.login, params: params, isShowLoadingView: true)
                .done { data in
                    let response = try self.decoder.decode(LoginModel.self, from: data)
                    if let token = response.token {
                        DataManager.shared.token = token
                    }
                    if let extraUserInfo = response.user_info {
                        DataManager.shared.extraUserInfo = extraUserInfo
                    }
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    func logout() -> Promise<LogoutMessage> {
        let params = [
            "device_id": UIDevice.current.identifierForVendor?.uuidString ?? ""
        ]
        
        return Promise<LogoutMessage> { seal in
            requestPostWithEndPoint(endPoint: EndPoint.Common.logout, params: params, isShowLoadingView: true)
                .done { data in
                    let response = try self.decoder.decode(LogoutMessage.self, from: data)
                    DataManager.shared.clearDataForLogout()
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Get user info
    ///
    /// - Returns: Promise with UserInfoModel
    func getUserInfo() -> Promise<UserInfoModel> {
        return Promise<UserInfoModel> { seal in
            getDataWithEndPoint(endPoint: EndPoint.Common.userInfo, isShowLoadingView: true)
                .done { data in
                    let response = try self.decoder.decode(UserInfoModel.self, from: data)
                    DataManager.shared.userInfo = response
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Get number of members
    ///
    /// - Returns: Promise with number of members
    func getMemberCount() -> Promise<MemberCount> {
        return Promise<MemberCount> { seal in
            getDataWithEndPoint(endPoint: EndPoint.Common.countMembers, isShowLoadingView: false)
                .done { data in
                    let response = try self.decoder.decode(MemberCount.self, from: data)
                    DataManager.shared.memberCount = response.count ?? 0
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Get all members
    ///
    /// - Returns: Promise with list of MemberModel
    func getMemberList() -> Promise<[MemberModel]> {
        return Promise<[MemberModel]> { seal in
            getDataWithEndPoint(endPoint: EndPoint.Common.accountsInfo, isShowLoadingView: false)
                .done { data in
                    let response = try self.decoder.decode([MemberModel].self, from: data)
                    DataManager.shared.memberList = response
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Get available picking points
    ///
    /// - Returns: Promise with array of PickingPointModel
    func getPickingPoints() -> Promise<[PickingPointModel]> {
        return Promise<[PickingPointModel]> { seal in
            getDataWithEndPoint(endPoint: EndPoint.Common.pickingPoints, isShowLoadingView: false)
                .done { data in
                    let response = try self.decoder.decode([PickingPointModel].self, from: data)
                    // TO-DO: Handle return data
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    func setFireBaseToken() {
        // TO-DO
    }
}
