//
//  APIClient+Pricing.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

extension APIClient {
    
    /// Get pricing based on destination
    ///
    /// - Parameter destination: destination
    /// - Returns: Promise with PriceModel
    func getPricing(lat: Double, lon: Double, distance: Double, moveTime: Double, payment: String, service: String) -> Promise<PriceModel> {
        let finalEndpoint = EndPoint.Pricing.getPricing
            + "?lat=\(lat)"
            + "&lon=\(lon)"
            + "&distance=\(distance)"
            + "&move_time=\(moveTime)"
            + "&payment=\(payment)"
            + "&service=\(service)"
        
        return Promise<PriceModel> { seal in
            getDataWithEndPoint(endPoint: finalEndpoint, isShowLoadingView: true)
                .done { data in
                    let response = try self.decoder.decode(PriceModel.self, from: data)
                    // TO-DO: Handle PriceModel data
                    DataManager.shared.pricing = response
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Get list of pricings
    ///
    /// - Parameters:
    ///   - lat: latitude
    ///   - lon: longitude
    ///   - distance: distance between
    ///   - moveTime: moving time
    ///   - payment: kind of payment
    /// - Returns: Promise with array of PriceModel
    func getListPricing(lat: Double, lon: Double, distance: Double, moveTime: Double, payment: String) -> Promise<[PriceModel]> {
        let finalEndpoint = EndPoint.Pricing.listPricings
            + "?lat=\(lat)"
            + "&lon=\(lon)"
            + "&distance=\(distance)"
            + "&move_time=\(moveTime)"
            + "&payment=\(payment)"
        
        return Promise<[PriceModel]> { seal in
            getDataWithEndPoint(endPoint: finalEndpoint, isShowLoadingView: true)
                .done { data in
                    let response = try self.decoder.decode([PriceModel].self, from: data)
                    // TO-DO: Handle PriceModel data
                    DataManager.shared.listPricing = response
                    seal.fulfill(response)
                }
                .catch { error in
                    seal.reject(error)
            }
        }
    }

}
