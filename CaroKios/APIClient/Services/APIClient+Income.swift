//
//  APIClient+Income.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

extension APIClient {
    /// Get income by booking point
    ///
    /// - Returns: Promise with IncomeModel
    func getIncomeByBookingPoint() -> Promise<IncomeModel> {
        return Promise<IncomeModel> { seal in
            getDataWithEndPoint(endPoint: EndPoint.Income.incomeByBooking, isShowLoadingView: true)
                .done { data in
                    let response = try self.decoder.decode(IncomeModel.self, from: data)
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
    
    /// Get income by account
    ///
    /// - Parameters:
    ///   - startDate: filter from this date
    ///   - endDate: filter to this date
    /// - Returns: Promise with IncomeModel
    func getIncomeByAccount(from startDate: String, to endDate: String) -> Promise<IncomeModel> {
        let finalEndPoint = EndPoint.Income.incomeByAccounts + "?from=\(startDate)" + "&to=\(endDate)"
        return Promise<IncomeModel> { seal in
            getDataWithEndPoint(endPoint: finalEndPoint, isShowLoadingView: true)
                .done { data in
                    let response = try self.decoder.decode(IncomeModel.self, from: data)
                    DataManager.shared.incomeList = response
                    seal.fulfill(response)
            }
                .catch { error in
                    seal.reject(error)
            }
        }
    }
}
