//
//  APIClient.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

enum HTTPMethodType {
    case POST
    case PUT
    case DELETE
}

class APIClient {
    static let shared = APIClient()
    private init() {}
    
    let decoder = JSONDecoder()
    
    // MARK: - GET and REQUEST
    // PUT request
    internal var putRequest: NSMutableURLRequest! {
        get {
            // Init post request
            let _putRequest = NSMutableURLRequest()
            _putRequest.httpMethod = "PUT"
            
            _putRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            return _putRequest
        }
    }
    
    // DELETE request
    internal var deleteRequest: NSMutableURLRequest! {
        get {
            // Init post request
            let _deleteRequest = NSMutableURLRequest()
            _deleteRequest.httpMethod = "DELETE"
            
            _deleteRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            return _deleteRequest
        }
    }
    
    // POST request
    internal var postRequest: NSMutableURLRequest! {
        get {
            // Init post request
            let _postRequest = NSMutableURLRequest()
            _postRequest.httpMethod = "POST"
            
            _postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //_postRequest.setValue("ios", forHTTPHeaderField: "Device-Agent")
            return _postRequest
        }
    }
    
    // GET request
    internal var getRequest: NSMutableURLRequest! {
        get {
            // Init get request
            let _getRequest = NSMutableURLRequest()
            _getRequest.httpMethod = "GET"
            
            _getRequest.setValue("vi", forHTTPHeaderField: "language")
            _getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //_getRequest.setValue("application/json, text/plain, */*", forHTTPHeaderField: "Accept")
            return _getRequest
        }
    }
    
    /// Handle loading view presentation
    ///
    /// - Parameter isShow: should show or not
    private func hanldeShowLoadingView(isShow: Bool) {
        if let viewController = UIApplication.shared.topViewController() {
            viewController.handleLoadingView(isShow: isShow)
        }
    }
    
    /// Get data with end point and handle loading
    ///
    /// - Parameters:
    ///   - host: host URL
    ///   - endPoint: end point for URL
    ///   - params: params
    ///   - isShowLoadingView: should show loading view or not
    ///   - isCanShowAlertError: can show alert or not
    /// - Returns: Promise
    public func getDataWithEndPoint(host: String? = nil, endPoint: String, params: [String : Any]? = nil, isShowLoadingView: Bool, isCanShowAlertError: Bool = true) -> Promise<Data> {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let headers: HTTPHeaders = [
            "Authorization": DataManager.shared.token
        ]
        
        if isShowLoadingView {
            self.hanldeShowLoadingView(isShow: true)
        }
        
        var baseURL = Host.alphaURL
        if let host_ = host {
            baseURL = host_
        }
        
        let url = baseURL + endPoint
        
        return Promise<Data> { seal in
            
            AF.request(url,
                       method: .get,
                       parameters: params,
                       headers: headers).responseData { response in
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        
                        if isShowLoadingView {
                            self.hanldeShowLoadingView(isShow: false)
                        }
                        
                        switch response.result {
                            
                        case .failure(let error):
                            seal.reject(error)
                            UIApplication.shared.topViewController()?.showAlertView(title: MS_TITLE_ALERT, message: self.getDisplayMessage(error: error), okTitle: "OK", cancelTitle: nil)
                            
                        case .success(let responseObject):
                            if let code = response.response?.statusCode, "\(code)".first == "2" || "\(code)".first == "3" {
                                seal.fulfill(responseObject)
                            } else {
                                guard isCanShowAlertError else { return }
                                var json = try? JSON(data: responseObject)
                                let messeage = json?["message"].string ?? API_MESSAGE.OTHER_ERROR
                                UIApplication.shared.topViewController()?.showAlertView(title: MS_TITLE_ALERT, message: messeage, okTitle: "Đồng ý", cancelTitle: nil)
                            }
                        }
            }
            
        }
    }
    
    /// Send request with end point
    ///
    /// - Parameters:
    ///   - host: custom server link
    ///   - endPoint: API endpoint
    ///   - params: input parameters
    ///   - isShowLoadingView: should show loading view or not
    ///   - httpType: method type
    ///   - jsonData: input data
    ///   - additionHeader: additional header
    ///   - isCanShowAlertError: should show alert or not
    ///   - isUpdateSensor: is update sensor or not
    /// - Returns: Promise with data
    public func requestPostWithEndPoint(host: String? = nil, endPoint: String, params: [String : Any], isShowLoadingView: Bool, httpType: HTTPMethodType = .POST, jsonData: Data? = nil, additionHeader: [String: String] = [:], isCanShowAlertError: Bool = true, isUpdateSensor: Bool = false) -> Promise<Data> {
        
        var mutableURLRequest = self.postRequest!
        
        switch httpType {
        case .POST:
            break
        case .DELETE:
            mutableURLRequest = self.deleteRequest
            break
        case .PUT:
            mutableURLRequest = self.putRequest
            break
        }
        
        for addHeader in additionHeader {
            mutableURLRequest.addValue(addHeader.value, forHTTPHeaderField: addHeader.key)
        }
        
        if !DataManager.shared.token.isEmpty {
            mutableURLRequest.addValue(DataManager.shared.token, forHTTPHeaderField: "token")
            
        }
        
        if isShowLoadingView {
            self.hanldeShowLoadingView(isShow: true)
        }
        
        var baseURL = Host.alphaURL
        if let host = host {
            baseURL = host
        }
        
        return Promise<Data> { seal in
            mutableURLRequest.url = URL(string: baseURL + endPoint)
            
            if let data = jsonData {
                mutableURLRequest.httpBody = data
            } else {
                mutableURLRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
            }
            
            AF.request(mutableURLRequest as URLRequest).responseData(completionHandler: { (response) in
                
                guard !isUpdateSensor else {
                    //print("Update sensor success")
                    return }
                
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    if isShowLoadingView {
                        self.hanldeShowLoadingView(isShow: false)
                    }
                    
                }
                
                let code = response.response?.statusCode ?? 0
                
                if "\(code)".first == "2" || "\(code)".first == "3" {
                    //Success
                    if let responseObject = response.data {
                        seal.fulfill(responseObject)
                    } else {
                        let data = Data(base64Encoded: "ok") ?? Data()
                        seal.fulfill(data)
                    }
                    return
                }
                
                if code == 401 {
                    //Token invalid
                    if let responseObject = response.data {
                        seal.fulfill(responseObject)
                    } else {
                        let data = Data(base64Encoded: "ok") ?? Data()
                        seal.fulfill(data)
                    }
                    return
                }
                
                var userInfo: JSON?
                if let data = response.data {
                    userInfo = try? JSON(data: data)
                }
                
                let error = NSError(domain: "", code: code, userInfo: userInfo?.dictionaryObject)
                seal.reject(error)
                guard isCanShowAlertError else {
                    return
                }
                
                self.setUpToastStyle()
                if let view = UIApplication.shared.topViewController()?.view {
                    view.makeToast(self.getDisplayMessage(error: error), duration: 2, position: .bottom, title: nil, image: nil, completion: nil, width: view.frame.width, height: 60)
                }
            })
        }
    }
    
    // MARK: - Handler
    
    /// Get error message
    ///
    /// - Parameter error: received error
    /// - Returns: message
    public func getDisplayMessage(error: Error) -> String {
        let error = error as NSError
        
        var errMessage = error.userInfo["message"] as? String ?? ""
        if error.code == 400 || errMessage.isEmpty {
            errMessage = API_MESSAGE.OTHER_ERROR
        }
        
        return errMessage
    }
    
    /// Set up toast style
    private func setUpToastStyle() {
        ToastManager.shared.style.backgroundColor = FOREGROUND_COLOR
        ToastManager.shared.style.cornerRadius = 0
        ToastManager.shared.style.maxHeightPercentage = 1
        ToastManager.shared.style.verticalPadding = 16
        ToastManager.shared.style.horizontalPadding = 16
        ToastManager.shared.style.messageFont = FONT_BODY_1
    }
}
