//
//  CaroKios-Bridging-Header.h
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

#ifndef CaroKios_Bridging_Header_h
#define CaroKios_Bridging_Header_h

@import UIKit;
@import PromiseKit;
@import Nuke;
@import SVProgressHUD;
@import IQKeyboardManager;
@import SwiftyJSON;
@import IBAnimatable;
@import Alamofire;
@import XLPagerTabStrip;

#endif /* CaroKios_Bridging_Header_h */
