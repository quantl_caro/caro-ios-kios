//
//  PickingPointModel.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

struct PickingPointModel: Codable {
    var uid: String?
    var created_at: Int?
    var updated_at: Int?
    var status: String?
    var broker_uid: String?
    var address_name: String?
    var address: String?
    var lat: Double?
    var lon: Double?
}
