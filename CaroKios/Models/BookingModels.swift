//
//  BookingModels.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

struct LocationModel: Codable {
    var longitude: Double?
    var latitude: Double?
}

struct SourceModel: Codable {
    var address: String?
    var location: LocationModel?
}

struct RequestBookingModel: Codable {
    var source: SourceModel?
    var service_code: String?
    var payment: String?
    var start_time: Int?
    var description: String?
    var destinations: [String]?
    var booking_type: String?
    var estimate_distance: Double?
    var estimate_time: Double?
}

struct ReBookingModel: Codable {
    var old_booking_uid: String?
    var source: SourceModel?
    var service_code: String?
    var payment: String?
    var start_time: Int?
    var description: String?
    var destinations: [String]?
    var booking_type: String?
    var estimate_distance: Double?
    var estimate_time: Double?
}

struct ResultBookingModel: Codable {
    var uid: String?
    var booking_type: String?
    var order_type: String?
    var permission_code: String?
    var service_code: String?
    var service_name: String?
    var estimate_distance: Double?
    var estimate_time: Double?
    var broker_point_uid: String?
    var broker_point_address_uid: String?
    var customer_uid: String?
    var customer_phone: String?
    var driver_uid: String?
    var status: String?
    var payment_status: Int?
    var total_price: Int?
    var price_without_promotion: Int?
    var unit_price: String?
    var rating: Int?
    var rating_description: String?
    var rating_customer: Int?
    var rating_customer_description: String?
    var is_payed: Int?
    var reason_cancel: String?
    var created_at: Int?
    var updated_at: Int?
    var driver_accepted_at: Int?
    var driver_comming_at: Int?
    var journey_started_at: Int?
    var arrived_at: Int?
    var payment_at: Int?
    var journey_finished_at: Int?
    var promotion: String?
    var driver_info: DriverInfo?
    var source: SourceModel?
    var destinations: [String]?
    var detail_prices: [DetailPrice]?
    var description: String?
    var total_count_down: Int?
    var current_count_down: Int?
}
