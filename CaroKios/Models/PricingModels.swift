//
//  PricingModel.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

struct DetailPrice: Codable {
    var type: String?
    var description: String?
    var value: Int?
    var add_type: String?
    var order: Int?
    var factor: Int?
}

struct PriceModel: Codable {
    var id: Int?
    var created_at: Int?
    var updated_at: Int?
    var unit: String?
    var service_permission: String?
    var fix_price: Int?
    var min_price: Int?
    var distance_min_price: Int?
    var time_price: Int?
    var move_price: Int?
    var distance_unit: String?
    var time_unit: String?
    var service_bill_code: String?
    var service_name: String?
    var service_code: String?
    var promotion_code: String?
    var need_pay_with_promotion: Int?
    var need_pay: Int?
    var estimate_by_clock: Int?
    var detail_price: [DetailPrice]?
}
