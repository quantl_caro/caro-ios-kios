//
//  UserModel.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

struct UserInfoModel: Codable {
    var uid: String?
    var status: String?
    var broker_point_uid: String?
    var name: String?
    var phone: String?
    var level: String?
    var address_name: String?
    var address: String?
    var lat: Double?
    var lon: Double?
    var commission_value: Int?
    var income_showed: String?
    var bank_name: String?
    var bank_number: String?
    var bank_agency: String?
}

struct ExtraUserInfoModel: Codable {
    var uid: String?
    var status: String?
    var broker_point_uid: String?
    var broker_point_name: String?
    var name: String?
    var phone: String?
    var avatar: String?
    var level: String?
    var address_uid: String?
    var address_name: String?
    var address: String?
    var lat: Double?
    var lon: Double?
    var commission_value: Int?
    var income_showed: String?
    var payment_type: String?
    var bank_name: String?
    var bank_number: String?
    var bank_agency: String?
    var commission_pay_point_name: String?
    var commission_pay_point_logo: String?
}

struct MemberModel: Codable {
    var uid: String?
    var created_at: Int?
    var upadted_at: Int?
    var status: String?
    var user_name: String?
    var avatar: String?
    var rating: Int?
    var name: String?
    var phone: String?
    var broker_point_uid: String?
    var level: String?
    var previous_member_uid: String?
    var commission_value: Int?
    var broker_point_address_uid: String?
    var income_showed: String?
    var bank_name: String?
    var bank_number: String?
    var bank_agency: String?
    var address_name: String?
    var address: String?
}

struct DriverInfo: Codable {
    var name: String?
    var phone: String?
    var avatar: String?
    var vehicle_id: String?
    var vehicle_type: String?
    var vehicle_model: String?
    var vehicle_icon: String?
    var rating: String?
    var call_sign: String?
}

struct MemberCount: Codable {
    var count: Int?
}
