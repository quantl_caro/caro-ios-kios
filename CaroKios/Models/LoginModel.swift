//
//  LoginModel.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

struct LoginModel: Codable {
    var token: String?
    var user_info: ExtraUserInfoModel?
}

struct LogoutMessage: Codable {
    var message: String?
}
