//
//  IncomeModels.swift
//  CaroKios
//
//  Created by Quan Tran on 8/13/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

struct IncomeDetailsModel: Codable {
    var name: String?
    var value: Int?
    var booking_total: Int?
    var booking_success: Int?
}

struct IncomeModel: Codable {
    var details: [IncomeDetailsModel]?
    var total: IncomeDetailsModel?
}

struct CombinedIncomeModel {
    var detail: IncomeDetailsModel?
    var address: String?
}
