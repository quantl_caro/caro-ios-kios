//
//  TabBarItem.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

enum TabBarItem {
    case firstItem, secondItem, thirdItem, fourthItem, fifthItem
    
    var isRoundedItem: Bool {
        return false
    }
}

extension TabBarItem {
    
    var title: String {
        if isRoundedItem {
            return ""
        }
        
        return ""
    }
    
    var isEnabled: Bool {
        return !isRoundedItem
    }
    
    var tag: Int {
        switch self {
        case .firstItem:
            return 1
        case .secondItem:
            return 2
        case .thirdItem:
            return 3
        case .fourthItem:
            return 4
        case .fifthItem:
            return 5
        }
    }
    
    var image: UIImage? {
        if isRoundedItem {
            return nil
        }
        
        return #imageLiteral(resourceName: "ic_tabbar_home")
    }
    
    var tabBarItem: UITabBarItem {
        let tabItem = UITabBarItem(title: title, image: image, tag: tag)
        
        tabItem.title = nil
        tabItem.setTitleTextAttributes([NSAttributedString.Key.font: FONT_CAPTION_2], for: .normal)
        tabItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        
        switch self {
        case .firstItem:
            
            tabItem.image = UIImage(named: "ic_tabbar_home")
            tabItem.selectedImage = UIImage(named: "ic_tabbar_home_selected")
            tabItem.title = "Trang chủ"
            
            break
        case .secondItem:
            tabItem.image = UIImage(named: "ic_tabbar_car")
            tabItem.selectedImage = UIImage(named: "ic_tabbar_car_selected")
            tabItem.title = "Lựa chọn"
            
            break
        case .thirdItem:
            tabItem.image = UIImage(named: "ic_tabbar_wave")
            tabItem.title = "Đặt xe"
            
            break
        case .fourthItem:
            tabItem.image = UIImage(named: "ic_tabbar_history")
            tabItem.selectedImage = UIImage(named: "ic_tabbar_history_selected")
            tabItem.title = "Lịch sử"
            
            break
        case .fifthItem:
            tabItem.image = UIImage(named: "ic_tabbar_account")
            tabItem.selectedImage = UIImage(named: "ic_tabbar_account_selected")
            tabItem.title = "Tài khoản"
            
            break
        }
        
        tabItem.isEnabled = isEnabled
        return tabItem
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .firstItem:
            return .cyan
        case .secondItem:
            return .green
        case .thirdItem:
            return .orange
        case .fourthItem:
            return .gray
        case .fifthItem:
            return .white
        }
    }
}
