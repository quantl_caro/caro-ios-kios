//
//  TabBarViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

class TabBarViewController: UITabBarController {

    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = PRIMARY_COLOR_1
        createTabbarControllers()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let newTabBarHeight = defaultTabBarHeight + 19.0
        
        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight
        tabBar.frame = newFrame
    }
    
    /// Create Tab bar controllers and navigation items
    private func createTabbarControllers() {
        let systemTags = [TabBarItem.firstItem, .secondItem, .thirdItem, .fourthItem, .fifthItem]
        let viewControllers = systemTags.compactMap { self.createController(for: $0) }
        
        self.viewControllers = viewControllers
    }
    
    /// Create controller for navigation item
    ///
    /// - Parameters:
    ///   - customTabBarItem: tab bar item
    ///   - tag: tag number
    /// - Returns: controller for navigation item
    private func createController(for customTabBarItem: TabBarItem) -> UIViewController? {
        switch customTabBarItem {
        case .firstItem:
            let naviVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewControllerNav") as! UINavigationController
            naviVC.tabBarItem = customTabBarItem.tabBarItem
            return naviVC
        case .secondItem:
            let viewController = UIViewController()
            viewController.title = customTabBarItem.title
            viewController.tabBarItem = customTabBarItem.tabBarItem
            viewController.view.backgroundColor = customTabBarItem.backgroundColor
            
            return UINavigationController(rootViewController: viewController)
        case .thirdItem:
            let viewController = UIViewController()
            viewController.title = customTabBarItem.title
            viewController.tabBarItem = customTabBarItem.tabBarItem
            viewController.view.backgroundColor = customTabBarItem.backgroundColor
            
            return UINavigationController(rootViewController: viewController)
        case .fourthItem:
            let naviVC = UIStoryboard(name: "History", bundle: nil).instantiateViewController(withIdentifier: "HistoryViewControllerNav") as! UINavigationController
            naviVC.tabBarItem = customTabBarItem.tabBarItem
            return naviVC
        case .fifthItem:
            let naviVC = UIStoryboard(name: "Account", bundle: nil).instantiateViewController(withIdentifier: "AccountViewControllerNav") as! UINavigationController
            naviVC.tabBarItem = customTabBarItem.tabBarItem
            return naviVC
        }
    }
}
