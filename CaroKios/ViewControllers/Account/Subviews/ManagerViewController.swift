//
//  ManagerViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/15/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class ManagerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var disabledView: UIView!
    @IBOutlet weak var disabledTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "LeftRightTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftRightTableViewCell")
        tableView.register(UINib(nibName: "TitleBodyTableViewCell", bundle: nil), forCellReuseIdentifier: "TitleBodyTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        configSubviews()
    }
    
    // MARK: - Table data source and delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.shared.memberCount + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = createFirstCell(at: indexPath)
            return cell
        default:
            let cell = createCell(at: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        roundBorder(tableView, willDisplay: cell, forRowAt: indexPath)
    }
    
    // MARK: - Additional settings
    
    /// Indicator for view in segment
    ///
    /// - Parameter pagerTabStripController: selected tab
    /// - Returns: info
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: SEG2)
    }
    
    /// Create first cell which contains member count
    ///
    /// - Parameter indexPath: indexpath of cell
    /// - Returns: cell
    private func createFirstCell(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LeftRightTableViewCell", for: indexPath) as? LeftRightTableViewCell else {
            return UITableViewCell()
        }
        
        cell.leftText.font = FONT_SUBTITLE_1
        cell.leftText.text = ACCOUNT_MNG_COUNT + "\(DataManager.shared.memberCount)"
        cell.middleText.isHidden = true
        cell.rightText.isHidden = true
        
        return cell
    }
    
    /// Create others cell which contain members infomation
    ///
    /// - Parameter indexPath: indexpath of cell
    /// - Returns: cell
    private func createCell(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TitleBodyTableViewCell", for: indexPath) as? TitleBodyTableViewCell else {
            return UITableViewCell()
        }
        
        cell.reset()
        
        if indexPath.row == (tableView.numberOfRows(inSection: 0) - 1) {
            cell.handleSeparatorView(shouldHide: true)
        } else {
            cell.handleSeparatorView(shouldHide: false)
        }
        
        let member = DataManager.shared.memberList[indexPath.row - 1]
        cell.checkImage.isHidden = true
        cell.titleLabel.font = FONT_SUBTITLE_1
        cell.bodyLabel.font = FONT_BODY_1
        cell.titleLabel.text = member.user_name
        cell.bodyLabel.text = member.address
        
        return cell
    }
    
    /// Config disabled view and show hide table and disabled view
    private func configSubviews() {
        disabledTitle.text = ACCOUNT_MNG_DISABLE
        disabledTitle.font = FONT_BODY_1
        
        tableView.isHidden = DataManager.shared.level != .LEVEL_1
        disabledView.isHidden = DataManager.shared.level == .LEVEL_1
    }
}
