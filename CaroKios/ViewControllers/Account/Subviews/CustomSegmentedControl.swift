//
//  CustomSegmentedControl.swift
//  CaroKios
//
//  Created by Quan Tran on 8/14/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class CustomSegmentedControl: UIView {
    private var segTitles: [String]!
    private var segButtons: [UIButton]!
    private var selectorView: UIView!
    
    var selectedIndex = 0
    
    convenience init(frame: CGRect, segTitles: [String]) {
        self.init(frame: frame)
        self.segTitles = segTitles
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configViews()
    }
    
    private func configViews() {
        configButtons()
        configSelectorView()
        configSegView()
    }
    
    private func configSegView() {
        let stack = UIStackView(arrangedSubviews: segButtons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    private func configSelectorView() {
        let selectorWidth = frame.width / CGFloat(segButtons.count)
        selectorView = UIView(frame: CGRect(x: 0, y: self.frame.height, width: selectorWidth, height: 2))
        selectorView.backgroundColor = PRIMARY_COLOR_1
        let selectorPosition = frame.width / CGFloat(segButtons.count) * CGFloat(selectedIndex)
        selectorView.frame.origin.x = selectorPosition
        addSubview(selectorView)
    }
    
    private func configButtons() {
        segButtons = [UIButton]()
        segButtons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for title in segTitles {
            let button = UIButton(type: .system)
            button.setTitle(title, for: .normal)
            button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
            segButtons.append(button)
        }
        segButtons[0].setTitleColor(PRIMARY_COLOR_1, for: .normal)
    }
    
    @objc func buttonAction(sender: UIButton) {
        for (index, btn) in segButtons.enumerated() {
            btn.setTitleColor(FOREGROUND_COLOR, for: .normal)
            if btn == sender {
                let selectorPosition = frame.width / CGFloat(segButtons.count) * CGFloat(index)
                UIView.animate(withDuration: 0.3) {
                    self.selectorView.frame.origin.x = selectorPosition
                }
                btn.setTitleColor(PRIMARY_COLOR_1, for: .normal)
                selectedIndex = index
            }
        }
    }
}
