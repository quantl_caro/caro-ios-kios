//
//  InfoTableViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/14/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class InfoTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    var userInfoModel: ExtraUserInfoModel?
    
    // IBOutlets
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageTableViewCell")
        tableView.register(UINib(nibName: "LeftRightTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftRightTableViewCell")
        tableView.register(UINib(nibName: "InfoTableFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "InfoTableFooterView")
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    // MARK: - Table data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if let paymentType = userInfoModel?.payment_type, !paymentType.isEmpty {
                return 7
            }
            return 6
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = cellForRowInFirstSection(at: indexPath)
            return cell
        case 1:
            let cell = cellForSecondSection(at: indexPath)
            return cell
        case 2:
            let cell = UITableViewCell()
            cell.addSubview(createLogoutButton())
            cell.backgroundColor = .clear
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = .clear
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 16
        default:
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 2:
            let footer = setUpContactFooter()
            
            return footer
        default:
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 2:
            return 176
        default:
            return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 2 {
            roundBorder(tableView, willDisplay: cell, forRowAt: indexPath)
        }
    }
    
    // MARK: - Additional table settings
    
    /// Indicator for view in segment
    ///
    /// - Parameter pagerTabStripController: selected tab
    /// - Returns: info
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: SEG1)
    }
    
    /// Create logout buton for last cell
    ///
    /// - Returns: logout button
    private func createLogoutButton() -> UIButton {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 52))
        btn.layer.cornerRadius = 8
        btn.backgroundColor = WHITE_COLOR_1
        btn.layer.borderColor = PRIMARY_COLOR_1.cgColor
        btn.layer.borderWidth = 1
        btn.addTarget(self, action: #selector(logout(sender:)), for: .touchUpInside)
        btn.titleLabel?.font = FONT_BODY_1
        btn.setTitle(LOGOUT, for: .normal)
        btn.setTitleColor(PRIMARY_COLOR_1, for: .normal)
        btn.titleLabel?.textAlignment = .center
        
        return btn
    }
    
    @objc func logout(sender: UIButton) {
        _ = APIClient.shared.logout()
            .done { _ in
                LoginManager.showLoginView()
        }
    }
    
    /// Create infomation cell for first section
    ///
    /// - Parameter indexPath: indexpath of cell
    /// - Returns: cell
    private func cellForRowInFirstSection(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as? ImageTableViewCell else {
            return UITableViewCell()
        }
        
        cell.reset()
        
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = ACCOUNT_USERNAME
            cell.bodyLabel.text = DataManager.shared.loginName
            cell.imgView.image = UIImage(named: "ic_account_username")
        case 1:
            cell.titleLabel.text = ACCOUNT_NAME
            cell.bodyLabel.text = userInfoModel?.name
            cell.imgView.image = UIImage(named: "ic_account_name")
        case 2:
            cell.titleLabel.text = ACCOUNT_POINT
            cell.bodyLabel.text = userInfoModel?.address_name
            cell.imgView.image = UIImage(named: "ic_account_point")
        case 3:
            cell.titleLabel.text = ACCOUNT_PAY
            cell.bodyLabel.text = userInfoModel?.commission_pay_point_name
            cell.imgView.image = UIImage(named: "ic_account_pay")
        case 4:
            cell.titleLabel.text = ACCOUNT_ADDRESS
            cell.bodyLabel.text = userInfoModel?.address
            cell.imgView.image = UIImage(named: "ic_account_address")
        case 5:
            cell.titleLabel.text = ACCOUNT_PHONE
            cell.bodyLabel.text = userInfoModel?.phone
            cell.imgView.image = UIImage(named: "ic_account_phone")
            cell.handleSeparatorView(shouldHide: tableView.numberOfRows(inSection: 0) == 6)
        case 6:
            cell.titleLabel.text = ACCOUNT_BANK + " \(userInfoModel?.bank_name ?? "")"
            cell.bodyLabel.text = DataManager.shared.loginName
            cell.imgView.image = UIImage(named: "ic_account_bank")
            cell.handleSeparatorView(shouldHide: true)
        default:
            return UITableViewCell()
        }
        
        return cell
    }
    
    /// Create cell for com section
    ///
    /// - Parameter indexPath: indexpath of cell
    /// - Returns: cell
    private func cellForSecondSection(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LeftRightTableViewCell", for: indexPath) as? LeftRightTableViewCell else {
            return UITableViewCell()
        }
        
        cell.reset()
        
        cell.leftText.font = FONT_BODY_2
        cell.leftText.text = ACCOUNT_COM_TITLE
        cell.middleText.font = FONT_HEADLING_5
        cell.middleText.text = "\(userInfoModel?.commission_value ?? 0)"
        cell.rightText.font = FONT_SUBTITLE_2
        cell.rightText.text = "đ/Cuốc"
        cell.handleSeparatorView(shouldHide: true)
        
        return cell
    }
    
    /// Config view for footer
    ///
    /// - Returns: footer
    private func setUpContactFooter() -> UIView {
        guard let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "InfoTableFooterView") as? InfoTableFooterView else {
            return UIView()
        }
        
        let attributedString1 = NSMutableAttributedString(string: SUPPORT_TEXT)
        let attributedString2 = NSMutableAttributedString(string: SUPPORT_CONTACT)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = .center
        attributedString1.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString1.length))
        attributedString2.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString2.length))
        
        footer.titleLabel.attributedText = attributedString1
        footer.contactLabel.attributedText = attributedString2
        footer.nameLabel.text = SUPPORT_NAME
        footer.titleLabel.font = FONT_CAPTION_1
        footer.nameLabel.font = FONT_CAPTION_2
        footer.contactLabel.font = FONT_CAPTION_1
        
        return footer
    }
}
