//
//  AccountViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/14/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class AccountViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var tabBarView: ButtonBarView!
    
    override func viewDidLoad() {
        setUpPagerTabStrip()
        super.viewDidLoad()
        
        setUpTitle()
        view.setNeedsLayout()
        view.layoutIfNeeded()
        buttonBarView.removeFromSuperview()
        tabBarView.addSubview(buttonBarView)
        buttonBarView.backgroundView?.backgroundColor = .clear
        buttonBarView.backgroundColor = .clear
    }
    
    // MARK: - UI Settings
    private func setUpTitle() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: FONT_HEADLING_5]
        self.navigationItem.title = ACCOUNT_TITLE
    }
    
    private func setUpPagerTabStrip() {
        tabBarView.layer.cornerRadius = 0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.selectedBarHeight = 2.0
        settings.style.selectedBarBackgroundColor = PRIMARY_COLOR_1
        settings.style.buttonBarHeight = 45
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarItemLeftRightMargin = 0
        settings.style.buttonBarItemFont = FONT_BODY_1
        settings.style.buttonBarItemBackgroundColor = WHITE_COLOR_1
        settings.style.buttonBarItemTitleColor = .black
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            newCell?.label.textColor = PRIMARY_COLOR_1
            
            oldCell?.label.textColor = .black
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let vc = UIStoryboard(name: "InfoTableViewController", bundle: nil).instantiateViewController(withIdentifier: "InfoTableViewController") as! InfoTableViewController
        vc.userInfoModel = DataManager.shared.extraUserInfo
        
        let vc2 = UIStoryboard(name: "ManagerViewController", bundle: nil).instantiateViewController(withIdentifier: "ManagerViewController") as! ManagerViewController
        
        return [vc, vc2]
    }
}
