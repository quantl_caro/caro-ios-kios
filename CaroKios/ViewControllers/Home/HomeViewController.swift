//
//  HomeViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

class HomeViewController: UIViewController {

    // IBOutlets
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var greetLabel: UILabel!
    @IBOutlet weak var kiosLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpEmptyView()
        setUpTitle()
        getAccountsData()
    }
    
    // MARK: - UI Settings
    /// Set up empty view
    private func setUpEmptyView() {
        let greetText = NSMutableAttributedString(string: "Xin chào Chao Xìn!")
        let kiosText = NSMutableAttributedString(string: "Điểm Môi giới: Mặt Trăng")
        let addressText = NSMutableAttributedString(string: "Địa chỉ: Somewhere in the Galaxy far far far far far far away...")
        
        let normalStyle = NSMutableParagraphStyle()
        normalStyle.lineSpacing = 8
        normalStyle.alignment = .center
        
        greetText.addAttribute(.paragraphStyle, value: normalStyle, range: NSMakeRange(0, greetText.length))
        greetText.addAttribute(.font, value: FONT_SUBTITLE_1, range: NSMakeRange(9, greetText.length - 10))
        kiosText.addAttribute(.paragraphStyle, value: normalStyle, range: NSMakeRange(0, kiosText.length))
        addressText.addAttribute(.paragraphStyle, value: normalStyle, range: NSMakeRange(0, addressText.length))
        
        greetLabel.font = FONT_BODY_1
        kiosLabel.font = FONT_BODY_1
        addressLabel.font = FONT_BODY_1
        
        greetLabel.attributedText = greetText
        kiosLabel.attributedText = kiosText
        addressLabel.attributedText = addressText
    }
    
    private func setUpTitle() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: FONT_HEADLING_5]
        self.navigationItem.title = HOME_TITLE
    }
    
    private func getAccountsData() {
        _ = APIClient.shared.getMemberCount()
        _ = APIClient.shared.getMemberList()
    }
}
