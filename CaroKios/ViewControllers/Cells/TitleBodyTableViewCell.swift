//
//  TitleBodyTableViewCell.swift
//  CaroKios
//
//  Created by Quan Tran on 8/15/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class TitleBodyTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// Handle separator
    ///
    /// - Parameter isHidden: should hide or not
    func handleSeparatorView(shouldHide isHidden: Bool) {
        self.separatorView.isHidden = isHidden
    }
    
    func reset() {
        separatorView.isHidden = false
        checkImage.isHidden = true
    }
}
