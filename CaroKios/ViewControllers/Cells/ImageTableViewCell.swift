//
//  ImageTableViewCell.swift
//  CaroKios
//
//  Created by Quan Tran on 8/14/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    // IBOutlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        reset()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// Handle separator
    ///
    /// - Parameter isHidden: should hide or not    
    func handleSeparatorView(shouldHide isHidden: Bool) {
        separatorView.isHidden = isHidden
    }
    
    func reset() {
        titleLabel.font = FONT_BODY_2
        titleLabel.textColor = TITLE_IMGCELL_COLOR
        bodyLabel.font = FONT_BODY_1
        bodyLabel.textColor = FOREGROUND_COLOR
        imgView.image = nil
        separatorView.isHidden = false
        self.backgroundColor = .clear
    }
}
