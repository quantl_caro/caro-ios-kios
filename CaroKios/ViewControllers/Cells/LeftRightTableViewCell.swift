//
//  LeftRightTableViewCell.swift
//  CaroKios
//
//  Created by Quan Tran on 8/15/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class LeftRightTableViewCell: UITableViewCell {

    // IBOutlets
    @IBOutlet weak var leftText: UILabel!
    @IBOutlet weak var rightText: UILabel!
    @IBOutlet weak var middleText: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// Handle separator
    ///
    /// - Parameter isHidden: should hide or not
    func handleSeparatorView(shouldHide isHidden: Bool) {
        self.separatorView.isHidden = isHidden
    }
    
    func reset() {
        middleText.isHidden = false
        separatorView.isHidden = false
        self.backgroundColor = .clear
    }
}
