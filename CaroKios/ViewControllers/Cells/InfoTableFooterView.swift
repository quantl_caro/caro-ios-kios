//
//  InfoTableFooterView.swift
//  CaroKios
//
//  Created by Quan Tran on 8/15/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

class InfoTableFooterView: UITableViewHeaderFooterView {
    
    // IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
