//
//  LoginViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/11/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var loginModal: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var note1Label: UILabel!
    @IBOutlet weak var note2Label: UILabel!
    @IBOutlet weak var note3Label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
    }
    
    // MARK: - UI settings
    /// Set up UI
    func setUpUI() {
        setUpTextField()
        setUpLoginBtn()
        setUpModal()
        setUpNoteLabel()
    }
    
    /// Set up for userName and password text fields
    private func setUpTextField() {
        titleLabel.font = FONT_HEADLING_4
        usernameTextField.placeholder = "Tên đăng nhập"
        usernameTextField.font = FONT_BODY_1
        passwordTextField.placeholder = "Mật khẩu"
        passwordTextField.font = FONT_BODY_1
    }
    
    /// Set up UI for login button
    private func setUpLoginBtn() {
        let layer0 = CAGradientLayer()
        layer0.colors = [
            UIColor(red: 0.55, green: 0.22, blue: 1, alpha: 1).cgColor,
            UIColor(red: 1, green: 0, blue: 0.8, alpha: 1).cgColor
        ]
        layer0.locations = [0.34, 1]
        layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1.43, b: 0.54, c: -0.54, d: 62.77, tx: 0.27, ty: -31.12))
        layer0.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.5*view.bounds.size.height)
        layer0.position = view.center
        loginBtn.layer.addSublayer(layer0)
        loginBtn.titleLabel?.font = FONT_SUBTITLE_1
    }
    
    /// Set up login modal
    private func setUpModal() {
//        loginModal.dropShadow()
        loginModal.layer.cornerRadius = 8
    }
    
    /// Set up notes
    private func setUpNoteLabel() {
        let attributedString1 = NSMutableAttributedString(string: NOTE_TEXT)
        let attributedString2 = NSMutableAttributedString(string: NOTE_CONTACT)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = .center
        attributedString1.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString1.length))
        attributedString2.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString2.length))
        
        note1Label.attributedText = attributedString1
        note3Label.attributedText = attributedString2
        note2Label.text = NOTE_NAME
        note1Label.font = FONT_CAPTION_1
        note2Label.font = FONT_CAPTION_2
        note3Label.font = FONT_CAPTION_1
    }
    
    // MARK: - Actions
    @IBAction func loginAction(_ sender: UIButton) {
        // TO-DO: Uncomment before release
//        let id = usernameTextField.text ?? ""
//        let pass = passwordTextField.text ?? ""
        let id = "quantl2"
        let pass = "123456"
        DataManager.shared.login(userName: id, password: pass)
    }
    
}

extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -5, height: 0)
        layer.shadowRadius = 3
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
