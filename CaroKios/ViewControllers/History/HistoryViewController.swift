//
//  HistoryViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/16/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    
    let details = DataManager.shared.combinedIncomeList
    
    // MARK: - Table data source and delegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageTableViewCell")
        tableView.register(UINib(nibName: "LeftRightTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftRightTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        dateLabel.font = FONT_BODY_3
        let currentMonth = DataManager.shared.currentSelectedMonth.toString(.custom(kDateFormat))
        dateLabel.text = "Tháng \(currentMonth)"
        setUpTitle()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //        return details.count == 0 ? 1 : details.count
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = cellForFirstSection(at: indexPath)
            cell.backgroundColor = PRIMARY_COLOR_1
            
            return cell
        default:
            let cell = cellForSecondSection(at: indexPath)
            cell.backgroundColor = .white
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        roundBorder(tableView, willDisplay: cell, forRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 16
        default:
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case tableView.numberOfSections - 1:
            return 83
        default:
            return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // MARK: - Button actions
    
    @IBAction func leftButtonAction(_ sender: UIButton) {
        if DataManager.shared.stepFromCurrentMonth != 2 {
            configSelectedDate(withMonth: -1)
        }
    }
    
    @IBAction func rightButtonAction(_ sender: Any) {
        if DataManager.shared.stepFromCurrentMonth != 0 {
            configSelectedDate(withMonth: 1)
        }
    }
    
    // MARK: - Addition UI settings
    
    private func setUpTitle() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: FONT_HEADLING_5]
        self.navigationItem.title = HISTORY_TITLE
    }
    
    private func configSelectedDate(withMonth month: Int) {
        DataManager.shared.currentSelectedMonth = DataManager.shared.currentSelectedMonth.dateByAddingMonths(month)
        DataManager.shared.stepFromCurrentMonth -= month
        let currentMonth = DataManager.shared.currentSelectedMonth.toString(.custom(kDateFormat))
        dateLabel.text = "Tháng \(currentMonth)"
    }
    
    private func cellForFirstSection(at indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as? ImageTableViewCell else {
                return UITableViewCell()
            }
            cell.reset()
            
            cell.imageView?.image = UIImage(named: "ic_coin")
            cell.titleLabel.font = FONT_SUBTITLE_1
            cell.titleLabel.text = HISTORY_TOTAL_TITLE
            cell.titleLabel.textColor = .white
            
            let value = "100000"
            let unit = "usd"
            let valueString = "\(value) \(unit)"
            let valueText = NSMutableAttributedString(string: valueString)
            valueText.addAttribute(.font, value: FONT_HEADLING_4, range: NSMakeRange(0, value.count))
            valueText.addAttribute(.font, value: FONT_BODY_3, range: NSMakeRange(valueString.count - unit.count, unit.count))
            cell.bodyLabel.attributedText = valueText
            cell.bodyLabel.textColor = .white
            cell.leadingConstraint.constant = 16
            
            return cell
        default:
            let value = indexPath.row == 1 ? "2000" : "200"
            let title = indexPath.row == 1 ? HISTORY_CELL_TOTAL : HISTORY_CELL_SUCCESS
            let cell = createInfoCell(at: indexPath, withTitle: title, andValue: value)
            cell.leftText.textColor = .white
            cell.rightText.textColor = .white
            cell.handleSeparatorView(shouldHide: indexPath.row == 2)
            
            return cell
        }
    }
    
    private func cellForSecondSection(at indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as? ImageTableViewCell else {
                return UITableViewCell()
            }
            cell.reset()
            
            let address = "Moon"
            cell.imageView?.image = UIImage(named: "ic_coin")
            cell.titleLabel.font = FONT_SUBTITLE_1
            cell.titleLabel.text = address
            cell.titleLabel.textColor = FOREGROUND_COLOR
            
            let value = "100000"
            let unit = "usd"
            let valueString = "\(value) \(unit)"
            let valueText = NSMutableAttributedString(string: valueString)
            valueText.addAttribute(.font, value: FONT_HEADLING_5, range: NSMakeRange(0, value.count))
            valueText.addAttribute(.font, value: FONT_BODY_3, range: NSMakeRange(valueString.count - unit.count, unit.count))
            cell.bodyLabel.attributedText = valueText
            cell.bodyLabel.textColor = FOREGROUND_COLOR
            cell.leadingConstraint.constant = 16
            
            return cell
        default:
            let value = indexPath.row == 1 ? "2000" : "200"
            let title = indexPath.row == 1 ? HISTORY_CELL_TOTAL : HISTORY_CELL_SUCCESS
            let cell = createInfoCell(at: indexPath, withTitle: title, andValue: value)
            cell.leftText.textColor = FOREGROUND_COLOR
            cell.rightText.textColor = FOREGROUND_COLOR
            cell.handleSeparatorView(shouldHide: indexPath.row == 2)
            
            return cell
        }
    }
    
    private func createInfoCell(at indexPath: IndexPath, withTitle title: String, andValue value: String) -> LeftRightTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LeftRightTableViewCell", for: indexPath) as? LeftRightTableViewCell else {
            return LeftRightTableViewCell()
        }
        
        cell.reset()
        
        cell.leftText.font = FONT_BODY_1
        cell.rightText.font = FONT_HEADLING_5
        cell.leftText.text = title
        cell.rightText.text = value
        cell.middleText.isHidden = true
        
        return cell
    }
}
