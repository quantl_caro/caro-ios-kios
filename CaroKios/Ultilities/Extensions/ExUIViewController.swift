//
//  ExUIViewController.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

extension UIViewController {
    
    /// Show alert
    ///
    /// - Parameters:
    ///   - title: title for alert
    ///   - message: message body
    ///   - okTitle: ok button's title
    ///   - cancelTitle: cancel button's button
    ///   - completion: completion
    func showAlertView(title: String, message: String, okTitle: String?, cancelTitle: String?, completion:((_ isPressedOK: Bool) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (okTitle != nil) {
            let okAction = UIAlertAction(title: okTitle, style: .default, handler: { (result: UIAlertAction) in
                completion?(true)
            })
            okAction.setValue(PRIMARY_COLOR_1, forKey: "titleTextColor")
            alert.addAction(okAction)
        }
        
        if (cancelTitle != nil) {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: { (result: UIAlertAction) in
                completion?(false)
            })
            cancelAction.setValue(PRIMARY_COLOR_1, forKey: "titleTextColor")
            alert.addAction(cancelAction)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /// Handle show and hide loading view
    ///
    /// - Parameter isShow: should show or not
    func handleLoadingView(isShow: Bool) {
        if isShow {
            self.showLoadingView()
        } else {
            self.hideLoadingView()
        }
    }
    
    /// Show loading view
    private func showLoadingView() {
        SVProgressHUD.show(withStatus: "Caro...")
        
    }
    
    /// Hide loading view
    private func hideLoadingView() {
        SVProgressHUD.dismiss()
    }
    
    /// Rounding border of table sections
    ///
    /// - Parameters:
    ///   - tableView: target table
    ///   - cell: table's cell
    ///   - indexPath: indexpath of cell
    func roundBorder(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Top Left Right Corners
        let maskPathTop = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 8.0, height: 8.0))
        let shapeLayerTop = CAShapeLayer()
        shapeLayerTop.frame = cell.bounds
        shapeLayerTop.path = maskPathTop.cgPath
        
        //Bottom Left Right Corners
        let maskPathBottom = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 8.0, height: 8.0))
        let shapeLayerBottom = CAShapeLayer()
        shapeLayerBottom.frame = cell.bounds
        shapeLayerBottom.path = maskPathBottom.cgPath
        
        //All Corners
        let maskPathAll = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomRight, .bottomLeft], cornerRadii: CGSize(width: 8.0, height: 8.0))
        let shapeLayerAll = CAShapeLayer()
        shapeLayerAll.frame = cell.bounds
        shapeLayerAll.path = maskPathAll.cgPath
        
        if (indexPath.row == 0 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1) {
            cell.layer.mask = shapeLayerAll
        }
        else if (indexPath.row == 0) {
            cell.layer.mask = shapeLayerTop
        }
        else if (indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1) {
            cell.layer.mask = shapeLayerBottom
        }
    }
}
