//
//  ExUIApplication.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

extension UIApplication {
    
    /// Get topViewController
    ///
    /// - Returns: top view controller
    func topViewController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            return topController
        }
        
        return nil
    }
}
