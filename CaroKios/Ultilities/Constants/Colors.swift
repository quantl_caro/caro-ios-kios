//
//  Colors.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

//MARK: Colors
let PINK_COLOR = UIColor(hexString: "#ff00cd")
let TEXT_NORMAL_COLOR = UIColor(hexString: "#424242")
let FOREGROUND_COLOR = UIColor(hexString: "#454F63")
let BACKGROUND_COLOR = UIColor(hexString: "#F0F0F0")
let TITLE_IMGCELL_COLOR = UIColor(hexString: "#838A91")

//Color from RGB
func colorRGB(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
}

//Primary Color
let PRIMARY_COLOR_1 = UIColor(hexString: "#8B37FE")
let PRIMARY_COLOR_2 = UIColor(hexString: "#974BFE")
let PRIMARY_COLOR_3 = UIColor(hexString: "#A25FFE")
let PRIMARY_COLOR_4 = UIColor(hexString: "#AE73FE")
let PRIMARY_COLOR_5 = UIColor(hexString: "#B987FE")
let PRIMARY_COLOR_6 = UIColor(hexString: "#C59BFF")
let PRIMARY_COLOR_7 = UIColor(hexString: "#D1AFFF")
let PRIMARY_COLOR_8 = UIColor(hexString: "#DCC3FF")
let PRIMARY_COLOR_9 = UIColor(hexString: "#E8D7FF")
let PRIMARY_COLOR_10 = UIColor(hexString: "#F3EBFF")
let PRIMARY_COLOR_11 = UIColor(hexString: "#532198")

//Secondary Color
let SECONDARY_COLOR_1 = UIColor(hexString: "#102A43")
let SECONDARY_COLOR_2 = UIColor(hexString: "#283F56")
let SECONDARY_COLOR_3 = UIColor(hexString: "#405569")
let SECONDARY_COLOR_4 = UIColor(hexString: "#586A7B")
let SECONDARY_COLOR_5 = UIColor(hexString: "#707F8E")
let SECONDARY_COLOR_6 = UIColor(hexString: "#8794A1")
let SECONDARY_COLOR_7 = UIColor(hexString: "#9FAAB4")
let SECONDARY_COLOR_8 = UIColor(hexString: "#B7BFC7")
let SECONDARY_COLOR_9 = UIColor(hexString: "#CFD4D9")
let SECONDARY_COLOR_10 = UIColor(hexString: "#E7EAEC")

//Supporting Color
let SUPPORTING_COLOR_1 = UIColor(hexString: "#ff00cd")
let SUPPORTING_COLOR_2 = UIColor(hexString: "#FAC9F6")
let SUPPORTING_COLOR_3 = UIColor(hexString: "#3ACCE1")
let SUPPORTING_COLOR_4 = UIColor(hexString: "#665EFF")
let SUPPORTING_COLOR_5 = UIColor(hexString: "#FFB900")
let SUPPORTING_COLOR_6 = UIColor(hexString: "#DBECFF")

//Success Color
let SUCCESS_COLOR_1 = UIColor(hexString: "#00DF09")

//DANGER COLOR
let DANGER_COLOR_1 = UIColor(hexString: "#FD0202")

//WHITE COLOR
let WHITE_COLOR_1 = UIColor(hexString: "#FFFFFF")

