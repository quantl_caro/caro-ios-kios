//
//  Fonts.swift
//  CaroKios
//
//  Created by Quan Tran on 8/11/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

//MARK: Font
let MAIN_FONT_NAME_REGULAR = "Gilroy-Regular"
let MAIN_FONT_NAME_BOLD = "Gilroy-Bold"

//Heading
let FONT_HEADLING_1 = UIFont(name: "Gilroy-Bold", size: 60) ?? UIFont.systemFont(ofSize: 60)
let FONT_HEADLING_2 = UIFont(name: "Gilroy-Bold", size: 48) ?? UIFont.systemFont(ofSize: 48)
let FONT_HEADLING_3 = UIFont(name: "Gilroy-Bold", size: 34) ?? UIFont.systemFont(ofSize: 34)
let FONT_HEADLING_4 = UIFont(name: "Gilroy-Semibold", size: 24) ?? UIFont.systemFont(ofSize: 24)
let FONT_HEADLING_5 = UIFont(name: "Gilroy-Semibold", size: 20) ?? UIFont.systemFont(ofSize: 20)


//SubTitle
let FONT_SUBTITLE_1 = UIFont(name: "Gilroy-Semibold", size: 17) ?? UIFont.systemFont(ofSize: 17)
let FONT_SUBTITLE_2 = UIFont(name: "Gilroy-Semibold", size: 15) ?? UIFont.systemFont(ofSize: 15)

//Body
let FONT_BODY_1 = UIFont(name: "Gilroy", size: 17) ?? UIFont.systemFont(ofSize: 17)
let FONT_BODY_2 = UIFont(name: "Gilroy", size: 15) ?? UIFont.systemFont(ofSize: 15)
let FONT_BODY_3 = UIFont(name: "Gilrow", size: 20) ?? UIFont.systemFont(ofSize: 20)

//Caption
let FONT_CAPTION_1 = UIFont(name: "Gilroy", size: 13) ?? UIFont.systemFont(ofSize: 13)
let FONT_CAPTION_2 = UIFont(name: "Gilroy-Semibold", size: 13) ?? UIFont.systemFont(ofSize: 13)
