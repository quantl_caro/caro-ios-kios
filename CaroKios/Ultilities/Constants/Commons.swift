//
//  Commons.swift
//  CaroKios
//
//  Created by Quan Tran on 8/12/19.
//  Copyright © 2019 Quan Tran. All rights reserved.
//

import Foundation

// MARK: - API Constant
enum API_MESSAGE {
    static let NO_INTERNET = "Vui lòng kiểm tra lại kết nối mạng của bạn"
    static let OTHER_ERROR = "Lỗi không xác định. Vui lòng thử lại sau"
    static let DATA_FORMART_ERROR = "Không xử lý được dữ liệu nhận được"
    static let INCORRECT_USERNAME_PASSWORD = "Sai tên đăng nhập hoặc mật khẩu"
}

enum BookingStatus {
    static let WAITING = "WAITING"
    static let DRIVER_COMMING = "DRIVER_COMMING"
    static let DRIVER_WAITING = "DRIVER_WAITING"
    static let TIMEOUT = "TIMEOUT"
}

enum BrokerLevel: String {
    case LEVEL_1 = "BROKER_POINT_USER_LEVEL_1"
    case LEVEL_2 = "BROKER_POINT_USER_LEVEL_2"
}

// MARK: - Keys
let kToken = "kToken"
let kUsername = "kUsername"
let kDateFormat = "MM/YYYY"

// MARK: - Titles
let ACCOUNT_TITLE = "Thông tin tài khoản"
let ACCOUNT_USERNAME = "Tên Đăng Nhập"
let ACCOUNT_NAME = "Họ Tên"
let ACCOUNT_POINT = "Điểm môi giới"
let ACCOUNT_PAY = "Đơn vị chi trả hoa hồng"
let ACCOUNT_ADDRESS = "Vị trí"
let ACCOUNT_PHONE = "Số điện thoại"
let ACCOUNT_BANK = "Ngân hàng"
let ACCOUNT_COM_TITLE = "Hoa hồng taxi thường"
let ACCOUNT_MNG_COUNT = "Số lượng: "
let ACCOUNT_MNG_DISABLE = "Bạn chưa có tính năng này"

let HOME_TITLE = "Danh sách đặt xe"
let SEG1 = "Thông tin"
let SEG2 = "Quản lý"

let LOGOUT = "Đăng xuất"

let NOTE_TEXT = "Bạn muốn trở thành điểm sảnh của Caro?\nVui lòng liên hệ bộ phận kinh doanh:"
let NOTE_NAME = "Lê Tuấn Anh"
let NOTE_CONTACT = "Phone: 0936131913\nEmail: marketing@g7taxi.vn"

let SUPPORT_TEXT = "Hỗ trợ kinh doanh"
let SUPPORT_NAME = "Lê Tuấn Anh"
let SUPPORT_CONTACT = "Phone: 0936131913\nEmail: marketing@g7taxi.vn"

let MS_TITLE_ALERT = "Thông báo"

let HISTORY_TITLE = "Lịch sử"
let HISTORY_TOTAL_TITLE = "Tổng số tiền"
let HISTORY_CELL_TOTAL = "Tổng số cuốc đặt"
let HISTORY_CELL_SUCCESS = "Số cuốc thành công"
